package com.ibm.coderally.agent;

public class ZmiennaLingwistycznaPredkosc {
	public double wolno(float f){
		if(f <= 50){
			return 1.0;
		}
		else if (f > 50 && f < 100){
			return -(f/50)+2;
		}
		else{
			return 0.0;
		}
		
	}
	
	public double srednio(float f){
		if(f <= 50 || f >= 200){
			return 0.0;
		}
		else if (f > 50 && f < 100){
			return (f/50)-1;
		}
		else if (f >= 100 && f <= 150){
			return 1.0;
		}
		else if (f > 150 && f < 200){
			return -(f/50) +4;
		}
		return 0.0;
		
	}

	public double szybko(float f){
		if(f <= 150){
			return 0.0;
		}
		else if (f > 150 && f < 200){
			return (f/50) -3;
		}
		else{
			return 1.0;
		}
		
	}

/*	public double wolno(float f){
		if(f <= 50){
			return 1.0;
		}
		else if (f > 50 && f < 100){
			return (100.0-f)/50;
		}
		else{
			return 0.0;
		}
		
	}*/

}
