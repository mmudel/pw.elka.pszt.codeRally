import com.ibm.coderally.agent.DefaultCarAIAgent;
import com.ibm.coderally.agent.Logika;
import com.ibm.coderally.agent.wartosci;
import com.ibm.coderally.api.agent.AIUtils;
import com.ibm.coderally.api.internal.CRFileLog;
import com.ibm.coderally.entity.cars.agent.Car;
import com.ibm.coderally.entity.obstacle.agent.Obstacle;
import com.ibm.coderally.geo.Vec2Utils;
import com.ibm.coderally.geo.agent.CheckPoint;
import com.ibm.coderally.track.agent.Track;

public class Rrrrr extends DefaultCarAIAgent {
	Logika log = new Logika();
	@Override
	public void onCarCollision(Car other) {
		// Provide custom logic or remove method for default implementation.		
	}

	@Override
	public void onCheckpointUpdated(CheckPoint oldCheckpoint) {
		// Replace with custom logic or remove method for default implementation.
		getCar().setBrakePercent(0);
		getCar().setAccelerationPercent(100);
		getCar().setTarget(AIUtils.getClosestLane(getCar().getCheckpoint(), getCar().getPosition()));
		
	}

 @Override
	public void onObstacleInProximity(Obstacle obstacle) {
		// Provide custom logic or remove method for default implementation.		
	}

	@Override
	public void onOffTrack() {
		// Provide custom logic or remove method for default implementation.		
	}

	@Override
	public void onOpponentInProximity(Car car) {
		if (AIUtils.isCarAhead(getTrack(), getCar(), car)) {
		float mojaPredkosc = Vec2Utils.magnitude(getCar().getVelocity());
		float przeciwnikPredkosc =  Vec2Utils.magnitude(car.getVelocity());
		System.err.println("Moja predkosc: " + mojaPredkosc);
		System.err.println("Predkosc przeciwnika: " + przeciwnikPredkosc);
		//regula 1
		//IF
		double mi1 = log.AND(log.predkosc.wolno(mojaPredkosc), log.predkosc.szybko(przeciwnikPredkosc));
		//THEN
		//System.err.println("mi mocno " + mi1);
		wartosci tmp1 =log.przyspieszenie.mocno(mi1);
		
		//regula 2
		//IF
		 double mi2 = log.AND(log.predkosc.szybko(mojaPredkosc), log.predkosc.szybko(przeciwnikPredkosc));
		//THEN
		wartosci tmp2 =log.przyspieszenie.slabo(mi2);
		
		//regula 3
		//IF
		 double mi3 = log.AND(log.predkosc.srednio(mojaPredkosc), log.predkosc.wolno(przeciwnikPredkosc));
		//THEN
		wartosci tmp3 =log.przyspieszenie.srednio(mi3);
		
		//agregacja regul i wyostrzanie metod� najwiekszego srodka
		
		// wybieramy najwieksza wartosc:
		int przyspieszenie;
		if(mi1 >= mi3 && mi1 >= mi2){
			//wyostrzanie
			 przyspieszenie = log.srodekMaksimum(tmp1);
			System.err.println("DECYZJA: przyspiesz MOCNO " + przyspieszenie);
		}
		else if(mi3 >= mi1 && mi3 >= mi2){
			//wyostrzanie
			 przyspieszenie = log.srodekMaksimum(tmp3);
			System.err.println("DECYZJA: przyspiesz SREDNIO " + przyspieszenie);
		}
		else  {
			//wyostrzanie
			 przyspieszenie = log.srodekMaksimum(tmp2);
			System.err.println("DECYZJA: przyspiesz SLABO " + przyspieszenie);
		}
		
		getCar().setTarget(car.getTarget());
		getCar().setAccelerationPercent(przyspieszenie);
		getCar().setBrakePercent(0);
		}
		
	}

	@Override
	public void onRaceStart() {

		// Replace with custom logic or remove method for default implementation.

		getCar().setBrakePercent(0);
		getCar().setAccelerationPercent(100);
		getCar().setTarget(AIUtils.getClosestLane(getCar().getCheckpoint(), getCar().getPosition()));
		
	}

	@Override
	public void onTimeStep() {
		// Replace with custom logic or remove method for default implementation.
		
		AIUtils.recalculateHeading(getCar());
	}

	@Override
	public void init(Car car, Track track) {
		// Provide custom logic or remove method for default implementation.		
	}

	@Override
	public void onObstacleCollision(Obstacle obstacle) {
		// Provide custom logic or remove method for default implementation.		
	}

	@Override
	public void onStalled() {
		// Provide custom logic or remove method for default implementation.		
	}

}
