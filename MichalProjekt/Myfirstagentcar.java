import java.util.ArrayList;
import java.util.List;

import com.ibm.coderally.agent.DefaultCarAIAgent;
import com.ibm.coderally.api.agent.AIUtils;
import com.ibm.coderally.entity.cars.agent.Car;
import com.ibm.coderally.entity.obstacle.agent.Obstacle;
import com.ibm.coderally.geo.Vec2Utils;
import com.ibm.coderally.geo.agent.CheckPoint;
import com.ibm.coderally.track.agent.Track;

import pw.elka.pszt.logika_rozmyta.reguly.RegulyOnCarCollision;
import pw.elka.pszt.logika_rozmyta.reguly.RegulyOnObstacleCollision;
import pw.elka.pszt.logika_rozmyta.reguly.RegulyOnObstacleInProximity;
import pw.elka.pszt.logika_rozmyta.reguly.RegulyOnOpponentInProximity;
import pw.elka.pszt.logika_rozmyta.utils.*;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.*;

public class Myfirstagentcar extends DefaultCarAIAgent {

	@Override
	public void onCarCollision(Car other) {
		RegulyOnCarCollision.wykonajReguly(getCar(), other, getTrack());
	}

	@Override
	public void onCheckpointUpdated(CheckPoint oldCheckpoint) {
		//getCar().setTarget(AIUtils.getClosestLane(getCar().getCheckpoint(), getCar().getPosition()));
	}

	
	public void onObstacleInProximity(Obstacle obstacle) {
	RegulyOnObstacleInProximity.wykonajReguly(getCar(), obstacle);
	}

	@Override
	public void onOffTrack() {		
		List<Double> predkosci = new ArrayList<Double>();
		List<Double> predkoscITrakcja = new ArrayList<Double>();
		
		double mojaPredkosc = Vec2Utils.magnitude(getCar().getVelocity());
		double mojaTrakcja = getCar().getAttributes().getTractionCoefficient();
		
		/** Regula 1
		 * IF((Bardzo Wolno || Wolno) & Slaba) THEN Przyspieszenie.BardzoSlabo, Hamowanie.BardzoSlabo
		 */
		
		predkosci.add(Predkosc.czyBardzoWolno(mojaPredkosc));
		predkosci.add(Predkosc.czyWolno(mojaPredkosc));
		predkoscITrakcja.add(SpojnikiLogiczne.OR(predkosci));
		predkoscITrakcja.add(AtrybutTrakcja.czySlaba(mojaTrakcja));
		
		double przyspBardzoSlabo1 = SpojnikiLogiczne.AND(predkoscITrakcja);
		double hamowanieBardzoSlabo1 = SpojnikiLogiczne.AND(predkoscITrakcja);
		predkosci.clear();
		predkoscITrakcja.clear();
		
		/** Regula 2
		 * IF(Srednio & Slaba) THEN Przyspieszenie.BardzoSlabo, Hamowanie.Srednio
		 */
		
		predkoscITrakcja.add(Predkosc.czySrednio(mojaPredkosc));
		predkoscITrakcja.add(AtrybutTrakcja.czySlaba(mojaTrakcja));
		
		double przyspBardzoSlabo2 = SpojnikiLogiczne.AND(predkoscITrakcja);
		double hamowanieSrednio1 = SpojnikiLogiczne.AND(predkoscITrakcja);
		predkosci.clear();
		predkoscITrakcja.clear();
		
		/** Regula 3
		 * IF((Szybko || Bardzo Szybko) & Slaba) THEN Przyspieszenie.BardzoSlabo, Hamowanie.Mocno
		 */
		
		predkosci.add(Predkosc.czySzybko(mojaPredkosc));
		predkosci.add(Predkosc.czyBardzoSzybko(mojaPredkosc));
		predkoscITrakcja.add(SpojnikiLogiczne.OR(predkosci));
		predkoscITrakcja.add(AtrybutTrakcja.czySlaba(mojaTrakcja));
		
		double przyspBardzoSlabo3 = SpojnikiLogiczne.AND(predkoscITrakcja);
		double hamowanieMocno1 = SpojnikiLogiczne.AND(predkoscITrakcja);
		predkosci.clear();
		predkoscITrakcja.clear();
		
		/** Regula 4
		 * IF(Bardzo Wolno & Srednia) THEN Przyspieszenie.Slabo, Hamowanie.BardzoSlabo
		 */
		
		predkoscITrakcja.add(Predkosc.czyBardzoWolno(mojaPredkosc));
		predkoscITrakcja.add(AtrybutTrakcja.czySrednia(mojaTrakcja));
		
		double przyspSlabo1 = SpojnikiLogiczne.AND(predkoscITrakcja);
		double hamowanieBardzoSlabo2 = SpojnikiLogiczne.AND(predkoscITrakcja);
		predkosci.clear();
		predkoscITrakcja.clear();
		
		/** Regula 5
		 * IF((Wolno || Srednio) & Srednia) THEN Przyspieszenie.BardzoSlabo, Hamowanie.BardzoSlabo
		 */
		
		predkosci.add(Predkosc.czyWolno(mojaPredkosc));
		predkosci.add(Predkosc.czySrednio(mojaPredkosc));
		predkoscITrakcja.add(SpojnikiLogiczne.OR(predkosci));
		predkoscITrakcja.add(AtrybutTrakcja.czySrednia(mojaTrakcja));
		
		double przyspBardzoSlabo4 = SpojnikiLogiczne.AND(predkoscITrakcja);
		double hamowanieBardzoSlabo3 = SpojnikiLogiczne.AND(predkoscITrakcja);
		predkosci.clear();
		predkoscITrakcja.clear();
		
		/** Regula 6
		 * IF(Szybko & Srednia) THEN Przyspieszenie.BardzoSlabo, Hamowanie.Srednio
		 */
		
		predkoscITrakcja.add(Predkosc.czySzybko(mojaPredkosc));
		predkoscITrakcja.add(AtrybutTrakcja.czySrednia(mojaTrakcja));
		
		double przyspBardzoSlabo5 = SpojnikiLogiczne.AND(predkoscITrakcja);
		double hamowanieSrednio2 = SpojnikiLogiczne.AND(predkoscITrakcja);
		predkosci.clear();
		predkoscITrakcja.clear();
		
		/** Regula 7
		 * IF(Bardzo Szybko & Srednia) THEN Przyspieszenie.BardzoSlabo, Hamowanie.Mocno
		 */
		
		predkoscITrakcja.add(Predkosc.czyBardzoSzybko(mojaPredkosc));
		predkoscITrakcja.add(AtrybutTrakcja.czySrednia(mojaTrakcja));
		
		double przyspBardzoSlabo6 = SpojnikiLogiczne.AND(predkoscITrakcja);
		double hamowanieMocno2 = SpojnikiLogiczne.AND(predkoscITrakcja);
		predkosci.clear();
		predkoscITrakcja.clear();
		
		/** Regula 8
		 * IF((Bardzo Wolno || Wolno) & Mocna) THEN Przyspieszenie.Srednio, Hamowanie.BardzoSlabo
		 */
		
		predkosci.add(Predkosc.czyBardzoWolno(mojaPredkosc));
		predkosci.add(Predkosc.czyWolno(mojaPredkosc));
		predkoscITrakcja.add(SpojnikiLogiczne.OR(predkosci));
		predkoscITrakcja.add(AtrybutTrakcja.czyMocna(mojaTrakcja));
		
		double przyspSrednio1 = SpojnikiLogiczne.AND(predkoscITrakcja);
		double hamowanieBardzoSlabo4 = SpojnikiLogiczne.AND(predkoscITrakcja);
		predkosci.clear();
		predkoscITrakcja.clear();
		
		/** Regula 9
		 * IF(Srednio & Mocna) THEN Przyspieszenie.Slabo, Hamowanie.BardzoSlabo
		 */
		
		predkoscITrakcja.add(Predkosc.czySrednio(mojaPredkosc));
		predkoscITrakcja.add(AtrybutTrakcja.czyMocna(mojaTrakcja));
		
		double przyspSlabo2 = SpojnikiLogiczne.AND(predkoscITrakcja);
		double hamowanieBardzoSlabo5 = SpojnikiLogiczne.AND(predkoscITrakcja);
		predkosci.clear();
		predkoscITrakcja.clear();
		
		/** Regula 10
		 * IF(Szybko & Mocna) THEN Przyspieszenie.BardzoSlabo, Hamowanie.Slabo
		 */
		
		predkoscITrakcja.add(Predkosc.czySzybko(mojaPredkosc));
		predkoscITrakcja.add(AtrybutTrakcja.czyMocna(mojaTrakcja));
		
		double przyspBardzoSlabo7 = SpojnikiLogiczne.AND(predkoscITrakcja);
		double hamowanieSlabo1 = SpojnikiLogiczne.AND(predkoscITrakcja);
		predkosci.clear();
		predkoscITrakcja.clear();
		
		/** Regula 11
		 * IF(Bardzo Szybko & Mocna) THEN Przyspieszenie.BardzoSlabo, Hamowanie.Srednio
		 */
		
		predkoscITrakcja.add(Predkosc.czyBardzoSzybko(mojaPredkosc));
		predkoscITrakcja.add(AtrybutTrakcja.czyMocna(mojaTrakcja));
		
		double przyspBardzoSlabo8 = SpojnikiLogiczne.AND(predkoscITrakcja);
		double hamowanieSrednio3 = SpojnikiLogiczne.AND(predkoscITrakcja);
		predkosci.clear();
		predkoscITrakcja.clear();
		
		/* Agregacja regul */
		
		double przyspieszenie, hamowanie;
		
		List<Double> bardzoSlaboPrzysp = new ArrayList<Double>(), slaboPrzysp = new ArrayList<Double>(), srednioPrzysp = new ArrayList<Double>(), mocnoPrzysp = new ArrayList<Double>(),
				bardzoSlaboHamowanie = new ArrayList<Double>(), slaboHamowanie = new ArrayList<Double>(), srednioHamowanie = new ArrayList<Double>(), mocnoHamowanie = new ArrayList<Double>();
		
		bardzoSlaboPrzysp.add(przyspBardzoSlabo1);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo2);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo3);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo4);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo5);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo6);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo7);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo8);
		slaboPrzysp.add(przyspSlabo1);
		slaboPrzysp.add(przyspSlabo2);
		srednioPrzysp.add(przyspSrednio1);
		
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo1);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo2);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo3);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo4);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo5);
		slaboHamowanie.add(hamowanieSlabo1);
		srednioHamowanie.add(hamowanieSrednio1);
		srednioHamowanie.add(hamowanieSrednio2);
		srednioHamowanie.add(hamowanieSrednio3);
		mocnoHamowanie.add(hamowanieMocno1);
		mocnoHamowanie.add(hamowanieMocno2);
		
		przyspieszenie = Przyspieszenie.agregacja(bardzoSlaboPrzysp, slaboPrzysp, srednioPrzysp, mocnoPrzysp);
		hamowanie = Hamowanie.agregacja(bardzoSlaboHamowanie, slaboHamowanie, srednioHamowanie, mocnoHamowanie);
		
		getCar().setAccelerationPercent((int) przyspieszenie);
		getCar().setBrakePercent((int) hamowanie);
		getCar().setTarget(getCar().getCheckpoint().getIntersectionPoint(getCar().getRotation(), getCar().getPosition()));
}

	@Override
	public void onOpponentInProximity(Car car) {
		RegulyOnOpponentInProximity.wykonajReguly(getCar(), car, getTrack());
	}

	@Override
	public void onRaceStart() {
		getCar().setBrakePercent(0);
		getCar().setAccelerationPercent(100);
		getCar().setTarget(AIUtils.getClosestLane(getCar().getCheckpoint(), getCar().getPosition()));
		
	}

	@Override
	public void onTimeStep() {
		getCar().setTarget(AIUtils.getClosestLane(getCar().getCheckpoint(), getCar().getPosition()));
		
		List<Double> dane = new ArrayList<Double>();
		double mojaPredkosc = Vec2Utils.magnitude(getCar().getVelocity());
		double ileMuszeSkrecic = getCar().calculateHeading(getCar().getTarget());
		
		//System.err.println( Vec2Utils.magnitude(getCar().getVelocity()) );
		/** Regula 1
		 * IF(Bardzo Wolno & Malo) THEN Przysp.Mocno, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czyBardzoWolno(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyMalo(ileMuszeSkrecic));
		double przyspMocno1 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo1 = SpojnikiLogiczne.AND(dane);
		dane.clear();
		
		/** Regula 2
		 * IF(Bardzo Wolno & Srednio) THEN Przysp.Mocno, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czyBardzoWolno(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czySrednio(ileMuszeSkrecic));
		double przyspMocno2 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo2 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 3
		 * IF(Bardzo Wolno & Duzo) THEN Przysp.Srednio, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czyBardzoWolno(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyDuzo(ileMuszeSkrecic));
		double przyspSrednio1 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo3 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 4
		 * IF(Bardzo Wolno & Bardzo Duzo) THEN Przysp.Slabo, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czyBardzoWolno(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyBardzoDuzo(ileMuszeSkrecic));
		double przyspSlabo1 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo4 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 5
		 * IF(Wolno & Malo) THEN Przysp.Mocno, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czyWolno(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyMalo(ileMuszeSkrecic));
		double przyspMocno3 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo5 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 6
		 * IF(Wolno & Srednio) THEN Przysp.Srednio, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czyWolno(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czySrednio(ileMuszeSkrecic));
		double przyspSrednio2 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo6 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 7
		 * IF(Wolno & Duzo) THEN Przysp.Srednio, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czyWolno(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyDuzo(ileMuszeSkrecic));
		double przyspSrednio3 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo7 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 8
		 * IF(Wolno & Bardzo Duzo) THEN Przysp.Bardzo Slabo, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czyWolno(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyBardzoDuzo(ileMuszeSkrecic));
		double przyspBardzoSlabo1 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo8 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 9
		 * IF(Srednio & Malo) THEN Przysp.Mocno, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czySrednio(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyMalo(ileMuszeSkrecic));
		double przyspMocno4 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo9 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 10
		 * IF(Srednio & Srednio) THEN Przysp.Srednio, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czySrednio(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czySrednio(ileMuszeSkrecic));
		double przyspSrednio4 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo10 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 11
		 * IF(Srednio & Duzo) THEN Przysp.Bardzo Slabo, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czySrednio(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyDuzo(ileMuszeSkrecic));
		double przyspBardzoSlabo2 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo11 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 12
		 * IF(Srednio & Bardzo Duzo) THEN Przysp.Bardzo Slabo, Hamowanie.Srednio
		 */
		dane.add(Predkosc.czySrednio(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyBardzoDuzo(ileMuszeSkrecic));
		double przyspBardzoSlabo3 = SpojnikiLogiczne.AND(dane);
		double hamowanieSrednio1 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 13
		 * IF(Szybko & Malo) THEN Przysp.Mocno, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czySzybko(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyMalo(ileMuszeSkrecic));
		double przyspMocno5 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo12 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 14
		 * IF(Szybko & Srednio) THEN Przysp.Slabo, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czySzybko(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czySrednio(ileMuszeSkrecic));
		double przyspSlabo2 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo13 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 15
		 * IF(Szybko & Duzo) THEN Przysp.Bardzo Slabo, Hamowanie.Srednio
		 */
		dane.add(Predkosc.czySzybko(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyDuzo(ileMuszeSkrecic));
		double przyspBardzoSlabo4 = SpojnikiLogiczne.AND(dane);
		double hamowanieSrednio2 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 16
		 * IF(Szybko & Bardzo Duzo) THEN Przysp.Bardzo Slabo, Hamowanie.Mocno
		 */
		dane.add(Predkosc.czySzybko(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyBardzoDuzo(ileMuszeSkrecic));
		double przyspBardzoSlabo5 = SpojnikiLogiczne.AND(dane);
		double hamowanieMocno1 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 17
		 * IF(Bardzo Szybko & Malo) THEN Przysp.Slabo, Hamowanie.Bardzo Slabo
		 */
		dane.add(Predkosc.czyBardzoSzybko(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyMalo(ileMuszeSkrecic));
		double przyspSlabo3 = SpojnikiLogiczne.AND(dane);
		double hamowanieBardzoSlabo14 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 18
		 * IF(Bardzo Szybko & Srednio) THEN Przysp.Bardzo Slabo, Hamowanie.Srednio
		 */
		dane.add(Predkosc.czyBardzoSzybko(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czySrednio(ileMuszeSkrecic));
		double przyspBardzoSlabo6 = SpojnikiLogiczne.AND(dane);
		double hamowanieSrednio3 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 19
		 * IF(Bardzo Szybko & Duzo) THEN Przysp.Bardzo Slabo, Hamowanie.Mocno
		 */
		dane.add(Predkosc.czyBardzoSzybko(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyDuzo(ileMuszeSkrecic));
		double przyspBardzoSlabo7 = SpojnikiLogiczne.AND(dane);
		double hamowanieMocno2 = SpojnikiLogiczne.AND(dane);
		dane.clear();

		/** Regula 20
		 * IF(Bardzo Szybko & Bardzo Duzo) THEN Przysp.Bardzo Slabo, Hamowanie.Mocno
		 */
		dane.add(Predkosc.czyBardzoSzybko(mojaPredkosc));
		dane.add(IleTrzebaSkrecic.czyBardzoDuzo(ileMuszeSkrecic));
		double przyspBardzoSlabo8 = SpojnikiLogiczne.AND(dane);
		double hamowanieMocno3 = SpojnikiLogiczne.AND(dane);
		dane.clear();
		
		/* Agregacja regul */
		
		double przyspieszenie, hamowanie;
		
		List<Double> bardzoSlaboPrzysp = new ArrayList<Double>(), slaboPrzysp = new ArrayList<Double>(), srednioPrzysp = new ArrayList<Double>(), mocnoPrzysp = new ArrayList<Double>(), bardzoSlaboHamowanie = new ArrayList<Double>(), slaboHamowanie = new ArrayList<Double>(), srednioHamowanie = new ArrayList<Double>(), mocnoHamowanie = new ArrayList<Double>();
		
		bardzoSlaboPrzysp.add(przyspBardzoSlabo1);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo2);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo3);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo4);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo5);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo6);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo7);
		bardzoSlaboPrzysp.add(przyspBardzoSlabo8);
		slaboPrzysp.add(przyspSlabo1);
		slaboPrzysp.add(przyspSlabo2);
		slaboPrzysp.add(przyspSlabo3);
		srednioPrzysp.add(przyspSrednio1);
		srednioPrzysp.add(przyspSrednio2);
		srednioPrzysp.add(przyspSrednio3);
		srednioPrzysp.add(przyspSrednio4);
		mocnoPrzysp.add(przyspMocno1);
		mocnoPrzysp.add(przyspMocno2);
		mocnoPrzysp.add(przyspMocno3);
		mocnoPrzysp.add(przyspMocno4);
		mocnoPrzysp.add(przyspMocno5);
		
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo1);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo2);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo3);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo4);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo5);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo6);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo7);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo8);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo9);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo10);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo11);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo12);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo13);
		bardzoSlaboHamowanie.add(hamowanieBardzoSlabo14);
		srednioHamowanie.add(hamowanieSrednio1);
		srednioHamowanie.add(hamowanieSrednio2);
		srednioHamowanie.add(hamowanieSrednio3);
		mocnoHamowanie.add(hamowanieMocno1);
		mocnoHamowanie.add(hamowanieMocno2);
		mocnoHamowanie.add(hamowanieMocno3);
		
		przyspieszenie = Przyspieszenie.agregacja(bardzoSlaboPrzysp, slaboPrzysp, srednioPrzysp, mocnoPrzysp);
		hamowanie = Hamowanie.agregacja(bardzoSlaboHamowanie, slaboHamowanie, srednioHamowanie, mocnoHamowanie);
		
		getCar().setAccelerationPercent((int) przyspieszenie);
		getCar().setBrakePercent((int) hamowanie);
		
		//AIUtils.recalculateHeading(getCar());
	}

	/*@Override
	public void init(Car car, Track track) {
		// Provide custom logic or remove method for default implementation.	
	}*/

	@Override
	public void onObstacleCollision(Obstacle obstacle) {
		RegulyOnObstacleCollision.wykonajReguly(getCar(), obstacle);
	}

	@Override
	public void onStalled() {
		getCar().setBrakePercent(0);
		getCar().setAccelerationPercent(100);
		getCar().setTarget(AIUtils.getClosestLane(getCar().getCheckpoint(), getCar().getPosition()));
		// Provide custom logic or remove method for default implementation.		
	}

}
