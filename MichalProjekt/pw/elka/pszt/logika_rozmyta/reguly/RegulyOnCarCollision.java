package pw.elka.pszt.logika_rozmyta.reguly;
import java.util.ArrayList;
import java.util.List;

import pw.elka.pszt.logika_rozmyta.utils.Lista;
import pw.elka.pszt.logika_rozmyta.utils.PozycjaReguly;
import pw.elka.pszt.logika_rozmyta.utils.SpojnikiLogiczne;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.AtrybutTrakcja;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.Predkosc;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.Przyspieszenie;

import com.ibm.coderally.api.agent.AIUtils;
import com.ibm.coderally.entity.cars.agent.Car;
import com.ibm.coderally.geo.Vec2Utils;
import com.ibm.coderally.track.agent.Track;
public class RegulyOnCarCollision {
  public static void wykonajReguly(Car myCar, Car car, Track track){
	  if (AIUtils.isCarAhead(track, myCar, car)) {
			List<Double> bardzoSlabePrzyspieszenie = new ArrayList<Double>(); 
			List<Double> slabePrzyspieszenie = new ArrayList<Double>(); 
			List<Double> sredniePrzyspieszenie = new ArrayList<Double>();
			List<Double> mocnePrzyspieszenie = new ArrayList<Double>();
			
			List<Double> dane = new ArrayList<Double>();
			List<Double> tmp = new ArrayList<Double>();
			
			double mojaPredkosc = Vec2Utils.magnitude(myCar.getVelocity());
			double mojaTrakcja = myCar.getAttributes().getTractionCoefficient();
			
			dane.add(Predkosc.czyBardzoWolno(mojaPredkosc));
			dane.add(Predkosc.czyWolno(mojaPredkosc));
			tmp.add(SpojnikiLogiczne.OR(dane));
			tmp.add(0.0);
			
			ArrayList<PozycjaReguly> regulyOmin = new ArrayList<PozycjaReguly>();
			/** Regula 1
			 * IF(Bardzo Wolno || Wolno && S�aba) THEN Przysp.Srednio i omin
			 */
			tmp.set(1, AtrybutTrakcja.czySlaba(mojaTrakcja));
		    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(tmp));
		    regulyOmin.add( new PozycjaReguly(Lista.SREDNIO, sredniePrzyspieszenie.size()-1)); // SREDNIO 0
			
			
			/** Regula 2
			 * IF( Bardzo Wolno || Wolno && Srednia) THEN Przysp.Mocno i omin
			 */
			tmp.set(1, AtrybutTrakcja.czySrednia(mojaTrakcja));
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(tmp));
		    regulyOmin.add(new PozycjaReguly(Lista.MOCNO,mocnePrzyspieszenie.size()-1) ); // Mocno 0
			
			/** Regula 3
			 * IF( Bardzo Wolno || Wolno && Mocna) THEN Przysp.Mocno i omin
			 */
			tmp.set(1, AtrybutTrakcja.czyMocna(mojaTrakcja));
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(tmp));
		    regulyOmin.add(new PozycjaReguly(Lista.MOCNO,mocnePrzyspieszenie.size()-1) ); //mocno 1
		    
			dane.clear();
			dane.add(Predkosc.czySrednio(mojaPredkosc));
			dane.add(0.0);
			
			/** Regula 4
			 * IF(Srednio && Slaba) THEN Przysp.srednio i omin
			 */
			dane.set(1, AtrybutTrakcja.czySlaba(mojaTrakcja));
		    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(dane));
		    regulyOmin.add(new PozycjaReguly(Lista.SREDNIO,sredniePrzyspieszenie.size()-1 )); //srednio 1
			
			/** Regula 5
			 * IF(Srednio && Srednia) THEN Przysp.srednio i omin
			 */
		    dane.set(1, AtrybutTrakcja.czySrednia(mojaTrakcja));
		    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(dane));
		    regulyOmin.add(new PozycjaReguly(Lista.SREDNIO,sredniePrzyspieszenie.size()-1) ); //srednio 2
			
			/** Regula 6
			 * IF(Srednio && Mocna) THEN Przysp.mocno i taranuj
			 */
		    dane.set(1, AtrybutTrakcja.czyMocna(mojaTrakcja));
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(dane));  // mocno 2
			
		    
		    dane.set(0,Predkosc.czySzybko(mojaPredkosc));
			
			/** Regula 7
			 * IF(Szybko && S�aba) THEN Przysp.Slabo i omin
			 */
		    
		    dane.set(1, AtrybutTrakcja.czySlaba(mojaTrakcja));
		    slabePrzyspieszenie.add( SpojnikiLogiczne.AND(dane));
		    regulyOmin.add(new PozycjaReguly(Lista.SLABO,slabePrzyspieszenie.size()-1)); //slabo 0
			
			/** Regula 8
			 * IF(Szybko && Srednia) THEN Przysp.Srednio i  taranuj
			 */
		    dane.set(1, AtrybutTrakcja.czySrednia(mojaTrakcja));
		    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(dane)); //srednio 3
			
			
			/** Regula 9
			 * IF(Szybko && Mocna) THEN Przysp.Srednio i  taranuj
			 */
		    dane.set(1, AtrybutTrakcja.czyMocna(mojaTrakcja));
		    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(dane)); //srednio 4
			
			
			/** Regula 10
			 * IF(Bardzo Szybko && Slaba) THEN Przysp.B.Slabo i  omin
			 */
			
		    dane.set(0,Predkosc.czyBardzoSzybko(mojaPredkosc));
		    dane.set(1, AtrybutTrakcja.czySlaba(mojaTrakcja));
		    bardzoSlabePrzyspieszenie.add( SpojnikiLogiczne.AND(dane));
		    regulyOmin.add(new PozycjaReguly(Lista.BSLABO,bardzoSlabePrzyspieszenie.size()-1) ); //bslabo 0
		    
		    /** Regula 11
			 * IF(Bardzo Szybko && Srednia || Mocna) THEN Przysp.Srednio i  taranuj
			 */
		    
		    tmp.clear();
		    dane.set(0,AtrybutTrakcja.czySrednia(mojaTrakcja));
		    dane.set(1, AtrybutTrakcja.czyMocna(mojaTrakcja));
			tmp.add(SpojnikiLogiczne.OR(dane));
			tmp.add(Predkosc.czyBardzoSzybko(mojaPredkosc));
			sredniePrzyspieszenie.add(SpojnikiLogiczne.AND(tmp)); // srednio 5
			
			double przyspieszenie = Przyspieszenie.agregacja(bardzoSlabePrzyspieszenie,slabePrzyspieszenie, sredniePrzyspieszenie, mocnePrzyspieszenie);
			
			myCar.setAccelerationPercent((int) przyspieszenie);
			myCar.setBrakePercent(0);
			
			PozycjaReguly tmpPoz = new PozycjaReguly(Przyspieszenie.ktoraLista, Przyspieszenie.ktoryIndekswLiscie);
			if(regulyOmin.contains(tmpPoz)){
				myCar.setTarget(AIUtils.getAlternativeLane(myCar.getCheckpoint(), myCar.getPosition()));
			}
			else {
				myCar.setTarget(car.getTarget());
			}
			Przyspieszenie.DEBUG = false;
	  	}
		else{
			myCar.setBrakePercent(0);
			myCar.setAccelerationPercent(100);
		}
  }
}
