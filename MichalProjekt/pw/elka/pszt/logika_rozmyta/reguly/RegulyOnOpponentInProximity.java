package pw.elka.pszt.logika_rozmyta.reguly;

import java.util.ArrayList;
import java.util.List;

import pw.elka.pszt.logika_rozmyta.utils.Lista;
import pw.elka.pszt.logika_rozmyta.utils.PozycjaReguly;
import pw.elka.pszt.logika_rozmyta.utils.SpojnikiLogiczne;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.AtrybutPrzyspieszenie;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.Predkosc;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.Przyspieszenie;

import com.ibm.coderally.api.agent.AIUtils;
import com.ibm.coderally.entity.cars.agent.Car;
import com.ibm.coderally.geo.Vec2Utils;
import com.ibm.coderally.track.agent.Track;

public class RegulyOnOpponentInProximity {
public static void wykonajReguly(Car myCar, Car car, Track track){
	if (AIUtils.isCarAhead(track, myCar, car)) {
			List<Double> bardzoSlabePrzyspieszenie = new ArrayList<Double>(); 
			List<Double> slabePrzyspieszenie = new ArrayList<Double>(); 
			List<Double> sredniePrzyspieszenie = new ArrayList<Double>();
			List<Double> mocnePrzyspieszenie = new ArrayList<Double>();
			
			List<Double> miSzybkoscMoja = new ArrayList<Double>();
			List<Double> miSzybkoscPrzeciw = new ArrayList<Double>();
			List<Double> miAgreg = new ArrayList<Double>();
			
			double mojaPredkosc = Vec2Utils.magnitude(myCar.getVelocity());
			double predkoscPrzeciwnika = Vec2Utils.magnitude(car.getVelocity());
			double mojePrzyspieszenie = myCar.getAttributes().getAcceleration();
			
			miSzybkoscMoja.add(Predkosc.czyBardzoWolno(mojaPredkosc));
			miSzybkoscMoja.add(Predkosc.czyWolno(mojaPredkosc));
			miSzybkoscPrzeciw.add(Predkosc.czyBardzoWolno(predkoscPrzeciwnika));
			miSzybkoscPrzeciw.add(Predkosc.czyWolno(predkoscPrzeciwnika));
			miAgreg.add(SpojnikiLogiczne.OR(miSzybkoscMoja));
			miAgreg.add(SpojnikiLogiczne.OR(miSzybkoscPrzeciw));
			miAgreg.add(0.0);
			
			double miAtrPrzyspS = AtrybutPrzyspieszenie.czySlabe(mojePrzyspieszenie); 
			double miAtrPrzyspSr = AtrybutPrzyspieszenie.czySrednie(mojePrzyspieszenie); 
			double miAtrPrzyspM = AtrybutPrzyspieszenie.czyMocne(mojePrzyspieszenie); 
			ArrayList<Double> miAgregAtrPrzysp= new ArrayList<Double>();
			miAgregAtrPrzysp.add(miAtrPrzyspM);
			miAgregAtrPrzysp.add(miAtrPrzyspSr);
			miAgregAtrPrzysp.add(miAtrPrzyspS);
			
			ArrayList<PozycjaReguly> regulyOmin = new ArrayList<PozycjaReguly>();
			
			/**
			 *  Posta� regul:
			 *  IF (( Wyrazenie dla predkosci wlasnej) && (Wyra�enie dla predkosci przeciwnika) && moj atrybut Przyspiesznie) THEN ustawienie mojego pedal gazu i strategia: omin lub wjed� w przeciwnika 
			 */
			
			
			/** Regula 1
			 * IF((Bardzo Wolno || Wolno) && (Bardzo Wolno || Wolno) && S�abe) THEN Przysp.Mocno i omin
			 */
			miAgreg.set(2, miAtrPrzyspS );
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			
			
			/** Regula 2
			 * IF((Bardzo Wolno || Wolno) && (Bardzo Wolno || Wolno) && Srednie) THEN Przysp.Mocno i omin
			 */
			miAgreg.set(2, miAtrPrzyspSr);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			
			/** Regula 3
			 * IF((Bardzo Wolno || Wolno) && (Bardzo Wolno || Wolno) && Mocne) THEN Przysp.Srednio i omin
			 */
			miAgreg.set(2, miAtrPrzyspM);
		    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    regulyOmin.add( new PozycjaReguly(Lista.SREDNIO, sredniePrzyspieszenie.size()-1)); 
		    
		    
			miAgreg.set(0, Predkosc.czySrednio(mojaPredkosc));
			
			/** Regula 4
			 * IF(Srednio && (Bardzo Wolno || Wolno) && Slabe) THEN Przysp.Srednio i omin
			 */
			miAgreg.set(2, miAtrPrzyspS);
		    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    regulyOmin.add( new PozycjaReguly(Lista.SREDNIO, sredniePrzyspieszenie.size()-1)); 
			
			/** Regula 5
			 * IF(Srednio && (Bardzo Wolno || Wolno) && Srednie) THEN Przysp.Slabo i omin
			 */
			miAgreg.set(2,miAtrPrzyspSr);
		    slabePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    regulyOmin.add( new PozycjaReguly(Lista.SLABO, slabePrzyspieszenie.size()-1)); 
			
			
			/** Regula 6
			 * IF(Srednio && (Bardzo Wolno || Wolno) && Mocne) THEN Przysp.Slabo i omin
			 */
			miAgreg.set(2,miAtrPrzyspM);
		    slabePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    regulyOmin.add( new PozycjaReguly(Lista.SLABO, slabePrzyspieszenie.size()-1)); 
			
		    
		    miAgreg.set(1, Predkosc.czySrednio(predkoscPrzeciwnika));
			
			/** Regula 7
			 *  IF(Srednio && Srednio && Slabe) THEN Przysp.Srednio i omin
			 */
		    
		    miAgreg.set(2, miAtrPrzyspS);
		    slabePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    regulyOmin.add( new PozycjaReguly(Lista.SLABO, slabePrzyspieszenie.size()-1)); 
			
			/** Regula 8
			*  IF(Srednio && Srednio && Srednie) THEN Przysp.Mocno i taranuj
			 */
		    
		    miAgreg.set(2, miAtrPrzyspSr);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
			
			/** Regula 9
			*  IF(Srednio && Srednio && Mocne) THEN Przysp.Srednio i taranuj
			*/
		    
		    miAgreg.set(2, miAtrPrzyspM);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
			
			
		    
		    
		   
		    miSzybkoscPrzeciw.clear();
		    miSzybkoscPrzeciw.add(Predkosc.czyBardzoWolno(predkoscPrzeciwnika));
		    miSzybkoscPrzeciw.add(Predkosc.czyWolno(predkoscPrzeciwnika));
		    miAgreg.set(1, SpojnikiLogiczne.OR(miSzybkoscPrzeciw));
		    miAgreg.set(2, SpojnikiLogiczne.OR(miAgregAtrPrzysp));
		    
			/** Regula 10
			 * IF((Szybko || Bardzo Szybko ) && (Bardzo Wolno || Wolno) && (Slabe || Srednie || Mocne)) THEN Przysp.B.Slabo i omin
			 */
		    miSzybkoscMoja.clear();
		    miSzybkoscMoja.add(Predkosc.czySzybko(mojaPredkosc));
		    miSzybkoscMoja.add(Predkosc.czyBardzoSzybko(mojaPredkosc));
		    miAgreg.set(0, SpojnikiLogiczne.OR(miSzybkoscMoja));
		    bardzoSlabePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    regulyOmin.add( new PozycjaReguly(Lista.SLABO, bardzoSlabePrzyspieszenie.size()-1)); 
		    
		    /** Regula 11
			 *  IF((Wolno || Bardzo Wolno ) && Srednio && (Slabe || Srednie || Mocne)) THEN Przysp.Mocno i omin
			 */
		    
		    miSzybkoscMoja.clear();
		    miSzybkoscMoja.add(Predkosc.czyWolno(mojaPredkosc));
		    miSzybkoscMoja.add(Predkosc.czyBardzoWolno(mojaPredkosc));
		    miAgreg.set(0, SpojnikiLogiczne.OR(miSzybkoscMoja));
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    
		    
		    
		    
		    miSzybkoscMoja.clear();
		    miSzybkoscMoja.add(Predkosc.czySzybko(mojaPredkosc));
		    miSzybkoscMoja.add(Predkosc.czyBardzoSzybko(mojaPredkosc));
		    miAgreg.set(0, SpojnikiLogiczne.OR(miSzybkoscMoja));
		    
		    /** Regula 12
			 *  IF((Szybko || Bardzo Szybko ) && Srednio && Slabe) THEN Przysp.Slabo i omin
			 */
		    
		    miAgreg.set(2, miAtrPrzyspS);
		    slabePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    regulyOmin.add( new PozycjaReguly(Lista.SLABO, slabePrzyspieszenie.size()-1)); 
		    
		    /** Regula 13
			 *  IF((Szybko || Bardzo Szybko ) && Srednio && Srednie) THEN Przysp.Mocno i taranuj
			 */
		    
		    miAgreg.set(2, miAtrPrzyspSr);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg)); 
		    
		    /** Regula 14
			 *  IF((Szybko || Bardzo Szybko ) && Srednio && Mocne) THEN Przysp.Srednio i taranuj
			 */
		    
		    miAgreg.set(2, miAtrPrzyspM);
		    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    
		    /** Regula 15
			 * IF((Wolno || Bardzo Wolno ) && (Bardzo Szybko || Szybko) && (Slabe || Srednie || Mocne)) THEN Przysp.Mocno i omin
		    */
		    
		    miSzybkoscMoja.clear();
		    miSzybkoscMoja.add(Predkosc.czyWolno(mojaPredkosc));
		    miSzybkoscMoja.add(Predkosc.czyBardzoWolno(mojaPredkosc));
		    miAgreg.set(0, SpojnikiLogiczne.OR(miSzybkoscMoja));
		    miSzybkoscPrzeciw.clear();
		    miSzybkoscPrzeciw.add(Predkosc.czyBardzoSzybko(predkoscPrzeciwnika));
		    miSzybkoscPrzeciw.add(Predkosc.czySzybko(predkoscPrzeciwnika));
		    miAgreg.set(1, SpojnikiLogiczne.OR(miSzybkoscPrzeciw));
		    miAgreg.set(2, SpojnikiLogiczne.OR(miAgregAtrPrzysp));
		    
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    
		    
		    
		    
		    miAgreg.set(0, Predkosc.czySrednio(mojaPredkosc));
		    

			/** Regula 16
			 * IF(Srednio && (Bardzo Szybko || Szybko) && Slabo) THEN Przysp.Mocno i omin
			 */
		    
		    miAgreg.set(2, miAtrPrzyspS);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    
			/** Regula 17
			 * IF(Srednio && (Bardzo Szybko || Szybko) && Srednio) THEN Przysp.Mocno i taranuj
			 */
		    
		    miAgreg.set(2, miAtrPrzyspSr);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    
		    /** Regula 18
			 * IF(Srednio && (Bardzo Szybko || Szybko) && Mocno) THEN Przysp.Srednio i taranuj
			 */
		    
		    miAgreg.set(2, miAtrPrzyspM);
		    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    
		    /** Regula 19
			 * IF((Bardzo Szybko || Szybko) && (Bardzo Szybko || Szybko) && (Slabe || Srednie)) THEN Przysp.Srednio i taranuj
			 */
		    
		    miSzybkoscMoja.clear();
		    miSzybkoscMoja.add(Predkosc.czySzybko(mojaPredkosc));
		    miSzybkoscMoja.add(Predkosc.czyBardzoSzybko(mojaPredkosc));
		    miAgreg.set(0, SpojnikiLogiczne.OR(miSzybkoscMoja));
		    miAgregAtrPrzysp.remove(0);
		    miAgreg.set(2,  SpojnikiLogiczne.OR(miAgregAtrPrzysp));
		    
		    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    
		    /** Regula 20
			 * IF((Bardzo Szybko || Szybko) && (Bardzo Szybko || Szybko) && Mocne) THEN Przysp.B.Slabo i taranuj
			 */
		    
		    miAgreg.set(2, miAtrPrzyspM);
		    bardzoSlabePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgreg));
		    
			double przyspieszenie = Przyspieszenie.agregacja(bardzoSlabePrzyspieszenie,slabePrzyspieszenie, sredniePrzyspieszenie, mocnePrzyspieszenie);
			
			myCar.setAccelerationPercent((int) przyspieszenie);
			myCar.setBrakePercent(0);
			
			PozycjaReguly tmpPoz = new PozycjaReguly(Przyspieszenie.ktoraLista, Przyspieszenie.ktoryIndekswLiscie);
			if(regulyOmin.contains(tmpPoz)){
				myCar.setTarget(AIUtils.getAlternativeLane(myCar.getCheckpoint(), myCar.getPosition()));
			}
			else {
				myCar.setTarget(car.getTarget());
			}
			}
		else{
			myCar.setBrakePercent(0);
			myCar.setAccelerationPercent(100);
		}
	}
}
