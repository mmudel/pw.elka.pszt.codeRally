package pw.elka.pszt.logika_rozmyta.reguly;

import java.util.ArrayList;
import java.util.List;

import pw.elka.pszt.logika_rozmyta.utils.Lista;
import pw.elka.pszt.logika_rozmyta.utils.PozycjaReguly;
import pw.elka.pszt.logika_rozmyta.utils.SpojnikiLogiczne;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.AtrybutWaga;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.Hamowanie;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.Predkosc;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.WielkoscPrzeszkody;

import com.ibm.coderally.api.agent.AIUtils;
import com.ibm.coderally.entity.cars.agent.Car;
import com.ibm.coderally.entity.obstacle.agent.Obstacle;
import com.ibm.coderally.geo.Vec2Utils;



public class RegulyOnObstacleInProximity {
public static void wykonajReguly(Car myCar, Obstacle obstacle ){
	
			//Hamowanie.DEBUG = true;
			List<Double> bardzoSlabeOpoznienie = new ArrayList<Double>(); 
			List<Double> slabeOpoznienie = new ArrayList<Double>(); 
			List<Double> srednieOpoznienie = new ArrayList<Double>();
			List<Double> mocneOpoznienie = new ArrayList<Double>();
			
			
			List<Double> miAgregacja = new ArrayList<Double>();
			
			
			// Wartosci zmiennych lingwistycznych
			double mojaPredkosc = Vec2Utils.magnitude(myCar.getVelocity());
			double mojaWaga = myCar.getAttributes().getWeight();
			double wielkoscPrzeszkodyX = obstacle.getSize().getWidth();
			double wielkoscPrzeszkodyY = obstacle.getSize().getHeight();
			double wielkoscPrzeszkody = Math.max(wielkoscPrzeszkodyX , wielkoscPrzeszkodyY);
			
			//Obliczanie na stale zawierania Mojej Predko�ci w zbiorach
			List<Double> miMojaPredkosc = new ArrayList<Double>();
			
			miMojaPredkosc.add(Predkosc.czyBardzoWolno(mojaPredkosc));
			miMojaPredkosc.add(Predkosc.czyWolno(mojaPredkosc));
			double miMojaPredkoscMala = SpojnikiLogiczne.OR(miMojaPredkosc);
			miMojaPredkosc.clear();
			
			double miMojaPredkoscSrednia = Predkosc.czySrednio(mojaPredkosc);
			
			miMojaPredkosc.add(Predkosc.czySzybko(mojaPredkosc));
			miMojaPredkosc.add(Predkosc.czyBardzoSzybko(mojaPredkosc));
			double miMojaPredkoscDuza = SpojnikiLogiczne.OR(miMojaPredkosc);
			miMojaPredkosc.clear();
			
			// Obliczanie na stale zawierania Wielkosci Przeszkody w zbiorach
			double miWielkoscPrzeszkodyMala = WielkoscPrzeszkody.czyMala(wielkoscPrzeszkody); 
			double miWielkoscPrzeszkodySrednia = WielkoscPrzeszkody.czySrednia(wielkoscPrzeszkody); 
			double miWielkoscPrzeszkodyDuza = WielkoscPrzeszkody.czyDuza(wielkoscPrzeszkody); 
			
			// Obliczanie na stale zawierania Wagi w zbiorach
			double miAtrybutWagaMala = AtrybutWaga.czyMala(mojaWaga); 
			double miAtrybutWagaSrednia = AtrybutWaga.czySrednia(mojaWaga); 
			double miAtrybutWagaDuza = AtrybutWaga.czyDuza(mojaWaga); 
			

			
			
			// Potrzebne do decyzji czy omijac czy taranowac
			ArrayList<PozycjaReguly> regulyTaranuj = new ArrayList<PozycjaReguly>();
			//ArrayList<PozycjaReguly> regulyTaranuj = new ArrayList<PozycjaReguly>();
			
			/**
			 *  Posta� regul:
			 *  IF (( Wyrazenie dla predkosci wlasnej) && (Wyra�enie dla wielkosci przeszkody) && moj atrybut waga) THEN ustawienie mojego pedal gazu i strategia: omin lub wjed� w przeciwnika 
			 */
			
			
			/** Regula 1
			 * IF((Bardzo Wolno || Wolno) && ( Mala ) && Mala) THEN Hamuj bardzo s�abo  i omn
			 */
			miAgregacja.add(miMojaPredkoscMala);
			miAgregacja.add(miWielkoscPrzeszkodyMala);
			miAgregacja.add(miAtrybutWagaMala);
			bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    
			
			/** Regula 2
			 * IF((Bardzo Wolno || Wolno) && (Mala) && Srednie) THEN Hamuj bardzo s�abo  i omn
			 */
		    miAgregacja.set(2,miAtrybutWagaSrednia);
		    bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));

			
		    
			/** Regula 3
			 * IF((Bardzo Wolno || Wolno) && (Mala) && Duza) THEN Hamuj bardzo s�abo  i taranuj
			 */
		    miAgregacja.set(2,miAtrybutWagaDuza);
		    bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.BSLABO, bardzoSlabeOpoznienie.size()-1)); 
		    
			
			/** Regula 4
			 * IF((Bardzo Wolno || Wolno) && (Srednia) && Mala) THEN Hamuj bardzo s�abo  i omin
			 */
		    miAgregacja.set(1,miWielkoscPrzeszkodySrednia);
		    miAgregacja.set(2,miAtrybutWagaMala);
		    bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));

			
			/** Regula 5
			 * IF((Bardzo Wolno || Wolno) && (Srednia) && Srednia) THEN Hamuj bardzo s�abo  i omin
			 */
		    miAgregacja.set(2,miAtrybutWagaSrednia);
		    bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));

			
			
			/** Regula 6
			 * IF((Bardzo Wolno || Wolno) && (Srednia) && Duza) THEN Hamuj bardzo s�abo  i omin
			 */
		    miAgregacja.set(2,miAtrybutWagaDuza);
		    bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));

			
		    
			
			/** Regula 7
			 *  IF((Bardzo Wolno || Wolno) && (Duza) && Mala) THEN Hamuj bardzo s�abo  i omin
			 */
		    
		    miAgregacja.set(1,miWielkoscPrzeszkodyDuza);
		    miAgregacja.set(2,miAtrybutWagaMala);
		    bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));

			
			/** Regula 8
			*  IF((Bardzo Wolno || Wolno) && (Duza) && Srednia) THEN Hamuj bardzo s�abo  i omin
			 */
		    
		    miAgregacja.set(2,miAtrybutWagaSrednia);
		    bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));

			
			/** Regula 9
			*  IF((Bardzo Wolno || Wolno) && (Duza) && Duza) THEN Hamuj bardzo s�abo  i omin
			*/
		    
		    miAgregacja.set(2,miAtrybutWagaDuza);
		    bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));

			
			
		    
		    /** Regula 10
			 * IF((Srednia) && ( Mala ) && Mala) THEN Hamuj bardzo s�abo  i  taranuj
			 */
		  
		    miAgregacja.set(0,miMojaPredkoscSrednia);
			miAgregacja.set(1,miWielkoscPrzeszkodyMala);
			miAgregacja.set(2,miAtrybutWagaMala);
			bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
			regulyTaranuj.add( new PozycjaReguly(Lista.BSLABO, bardzoSlabeOpoznienie.size()-1)); 
		    
		    /** Regula 11
			 *  IF((Srednia) && ( Mala ) && Srednia) THEN Hamuj bardzo s�abo  i  taranuj
			 */
		    
			miAgregacja.set(2,miAtrybutWagaSrednia);
			bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
			regulyTaranuj.add( new PozycjaReguly(Lista.BSLABO, bardzoSlabeOpoznienie.size()-1)); 
		    
		    /** Regula 12
			 *  IF((Srednia) && ( Mala ) && Duza) THEN Hamuj bardzo s�abo  i  taranuj
			 */
		    
			miAgregacja.set(2,miAtrybutWagaDuza);
			bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
			regulyTaranuj.add( new PozycjaReguly(Lista.BSLABO, bardzoSlabeOpoznienie.size()-1)); 
		    /** Regula 13
			 *  IF((Srednia) && ( Srednia ) && Mala) THEN Hamuj srednio  i omin
			 */
		    
			 miAgregacja.set(1,miWielkoscPrzeszkodySrednia);
			 miAgregacja.set(2,miAtrybutWagaMala);
			 srednieOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    
		    /** Regula 14
			 *  IF((Srednia) && ( Srednia ) && Srednio) THEN Hamuj  s�abo  i omin
			 */
		    
			miAgregacja.set(2,miAtrybutWagaSrednia);
			slabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    
		    /** Regula 15
			 * IF((Srednia) && ( Srednia ) && Duza) THEN Hamuj bardzo s�abo  i  taranuj
		    */
		    
			miAgregacja.set(2,miAtrybutWagaDuza);
			bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
			regulyTaranuj.add( new PozycjaReguly(Lista.BSLABO, bardzoSlabeOpoznienie.size()-1)); 
			/** Regula 16
			 * IF((Srednia) && ( Duza ) && Mala) THEN Hamuj mocno i  omin
			 */
		    
			miAgregacja.set(1,miWielkoscPrzeszkodyDuza);
			miAgregacja.set(2,miAtrybutWagaMala);
		    mocneOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    
			/** Regula 17
			 * IF((Srednia) && ( Duza ) && Srednia) THEN Hamuj mocno i  omin
			 */
		    miAgregacja.set(2,miAtrybutWagaSrednia);
		    mocneOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    
		    /** Regula 18
			 * IF((Srednia) && ( Duza ) && Duza) THEN Hamuj srednio i  omin
			 */
		    
		    miAgregacja.set(2,miAtrybutWagaDuza);
		    srednieOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    
		    /** Regula 19
			 * IF((Bardzo Szybko || Szybko) && ( Mala ) && Mala) THEN Hamuj bardzo s�abo  i  taranuj
			 */
		    
		    miAgregacja.set(0,miMojaPredkoscDuza);
			miAgregacja.set(1,miWielkoscPrzeszkodyMala);
			miAgregacja.set(2,miAtrybutWagaMala);
			bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.BSLABO, bardzoSlabeOpoznienie.size()-1)); 
		    /** Regula 20
			 * IF((Bardzo Szybko || Szybko) && ( Mala ) && Srednia) THEN Hamuj bardzo s�abo  i  taranuj
			 */
		    
		    miAgregacja.set(2,miAtrybutWagaSrednia);
		    bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.BSLABO, bardzoSlabeOpoznienie.size()-1)); 
		    /** Regula 21
			 * IF((Bardzo Szybko || Szybko) && ( Mala ) && Duza) THEN Hamuj bardzo s�abo  i  taranuj
			 */
		    miAgregacja.set(2,miAtrybutWagaDuza);
		    bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.BSLABO, bardzoSlabeOpoznienie.size()-1)); 
		    /** Regula 22
			 * IF((Bardzo Szybko || Szybko) && ( Srednia ) && Mala) THEN Hamuj mocno i omin
			 */
		    miAgregacja.set(1,miWielkoscPrzeszkodySrednia);
			miAgregacja.set(2,miAtrybutWagaMala);
			mocneOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    
		    /** Regula 23
			 * IF((Bardzo Szybko || Szybko) && ( Srednia ) && Srednia) THEN Hamuj �rednio i omin
			 */
		    miAgregacja.set(2,miAtrybutWagaSrednia);
		    srednieOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    
		    /** Regula 24
			 * IF((Bardzo Szybko || Szybko) && ( Srednia ) && Duza) THEN Hamuj bardzo s�abo  i  taranuj
			 */
		    miAgregacja.set(2,miAtrybutWagaDuza);
		    bardzoSlabeOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.BSLABO, bardzoSlabeOpoznienie.size()-1)); 
		    /** Regula 25
			 * IF((Bardzo Szybko || Szybko) && ( Duza) && Mala) THEN Hamuj mocno i min
			 */
		    miAgregacja.set(1,miWielkoscPrzeszkodyDuza);
			miAgregacja.set(2,miAtrybutWagaMala);
			mocneOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
			 
		    /** Regula 26
			 * IF((Bardzo Szybko || Szybko) && ( Duza ) && Srednia) THEN Hamuj mocno i omin
			 */
			 miAgregacja.set(2,miAtrybutWagaSrednia);
			 mocneOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
			 
		    /** Regula 27
			 * IF((Bardzo Szybko || Szybko) && ( Duza ) && Duza) THEN Hamuj mocno i omin
			 */
			 miAgregacja.set(2,miAtrybutWagaDuza);
			 mocneOpoznienie.add( SpojnikiLogiczne.AND(miAgregacja));
			 
			 
			double opoznienie = Hamowanie.agregacja(bardzoSlabeOpoznienie,slabeOpoznienie, srednieOpoznienie, mocneOpoznienie);
			
			myCar.setBrakePercent((int) opoznienie);
			

			PozycjaReguly tmpPoz = new PozycjaReguly(Hamowanie.ktoraLista, Hamowanie.ktoryIndekswLiscie);
			if(regulyTaranuj.contains(tmpPoz)){
				myCar.setTarget(AIUtils.getClosestLane(myCar.getCheckpoint(), myCar.getPosition()));
					//System.err.println("hit " + tmpPoz.lista  ); 
			}
			else {
				myCar.setTarget(AIUtils.getAlternativeLane(myCar.getCheckpoint(), myCar.getPosition()));
					//System.err.println("avoid" + tmpPoz.lista  ); 
			}
			Hamowanie.DEBUG = false;
		
		}
}

