package pw.elka.pszt.logika_rozmyta.reguly;

import java.util.ArrayList;
import java.util.List;

import pw.elka.pszt.logika_rozmyta.utils.Lista;
import pw.elka.pszt.logika_rozmyta.utils.PozycjaReguly;
import pw.elka.pszt.logika_rozmyta.utils.SpojnikiLogiczne;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.AtrybutWaga;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.Predkosc;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.Przyspieszenie;
import pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne.WielkoscPrzeszkody;

import com.ibm.coderally.api.agent.AIUtils;
import com.ibm.coderally.entity.cars.agent.Car;
import com.ibm.coderally.entity.obstacle.agent.Obstacle;
import com.ibm.coderally.geo.Vec2Utils;



public class RegulyOnObstacleCollision {
public static void wykonajReguly(Car myCar, Obstacle obstacle ){
	
			Przyspieszenie.DEBUG = true;
			List<Double> bardzoSlabePrzyspieszenie = new ArrayList<Double>(); 
			List<Double> slabePrzyspieszenie = new ArrayList<Double>(); 
			List<Double> sredniePrzyspieszenie = new ArrayList<Double>();
			List<Double> mocnePrzyspieszenie = new ArrayList<Double>();
			
			
			List<Double> miAgregacja = new ArrayList<Double>();
			
			
			// Wartosci zmiennych lingwistycznych
			double mojaPredkosc = Vec2Utils.magnitude(myCar.getVelocity());
			double mojaWaga = myCar.getAttributes().getWeight();
			double wielkoscPrzeszkodyX = obstacle.getSize().getWidth();
			double wielkoscPrzeszkodyY = obstacle.getSize().getHeight();
			double wielkoscPrzeszkody = Math.max(wielkoscPrzeszkodyX , wielkoscPrzeszkodyY);
			
			//Obliczanie na stale zawierania Mojej Predko�ci w zbiorach

			double miMojaPredkoscSrednia = Predkosc.czyBardzoWolno(mojaPredkosc);
			double miMojaPredkoscBardzoMala = Predkosc.czyWolno(mojaPredkosc);
			double miMojaPredkoscMala = Predkosc.czySrednio(mojaPredkosc);
			double miMojaPredkoscDuza = Predkosc.czySzybko(mojaPredkosc);
			double miMojaPredkoscBardzoDuza = Predkosc.czyBardzoSzybko(mojaPredkosc);
			
			// Obliczanie na stale zawierania Wielkosci Przeszkody w zbiorach
			double miWielkoscPrzeszkodyMala = WielkoscPrzeszkody.czyMala(wielkoscPrzeszkody); 
			double miWielkoscPrzeszkodySrednia = WielkoscPrzeszkody.czySrednia(wielkoscPrzeszkody); 
			double miWielkoscPrzeszkodyDuza = WielkoscPrzeszkody.czyDuza(wielkoscPrzeszkody); 
			
			// Obliczanie na stale zawierania Wagi w zbiorach
			double miAtrybutWagaMala = AtrybutWaga.czyMala(mojaWaga); 
			double miAtrybutWagaSrednia = AtrybutWaga.czySrednia(mojaWaga); 
			double miAtrybutWagaDuza = AtrybutWaga.czyDuza(mojaWaga); 
			
			System.err.println("BS" + miMojaPredkoscBardzoMala  + "S" + miMojaPredkoscMala + "Sr" +  miMojaPredkoscSrednia + "D" +  miMojaPredkoscDuza + "BD" +  miMojaPredkoscBardzoDuza ); 
			System.err.println("M" + miWielkoscPrzeszkodyMala  + "S" + miWielkoscPrzeszkodySrednia + "D" +  miWielkoscPrzeszkodyDuza );
			System.err.println("M" + miAtrybutWagaMala  + "S" + miAtrybutWagaSrednia + "D" +  miAtrybutWagaDuza );
			// Potrzebne do decyzji czy omijac czy taranowac
			ArrayList<PozycjaReguly> regulyOmin = new ArrayList<PozycjaReguly>();
			ArrayList<PozycjaReguly> regulyTaranuj = new ArrayList<PozycjaReguly>();
			
			/**
			 *  Posta� regul:
			 *  IF (( Wyrazenie dla predkosci wlasnej) && (Wyra�enie dla wielkosci przeszkody) && moj atrybut waga) THEN ustawienie mojego pedal gazu i strategia: omin lub wjed� w przeciwnika 
			 */
			
			
			/** Regula 1
			 * IF((Bardzo Wolno) && ( Mala ) && Mala) THEN Przysp.Mocno i omin
			 */
			miAgregacja.add(miMojaPredkoscBardzoMala);
			miAgregacja.add(miWielkoscPrzeszkodyMala);
			miAgregacja.add(miAtrybutWagaMala);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			
			
			/** Regula 2
			 * IF((Bardzo Wolno) && (Mala) && Srednie) THEN Przysp.Mocno i omin
			 */
		    miAgregacja.set(2,miAtrybutWagaSrednia);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			
		    
		    

			/** Regula 3
			 * IF((Bardzo Wolno) && (Mala) && Duza) THEN Przysp.Mocno i taranuj
			 */
		    miAgregacja.set(2,miAtrybutWagaDuza);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    
			
			/** Regula 4
			 * IF((Bardzo Wolno) && (Srednia) && Mala) THEN Przysp.Mocno i omin
			 */
		    miAgregacja.set(1,miWielkoscPrzeszkodySrednia);
		    miAgregacja.set(2,miAtrybutWagaMala);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			
			/** Regula 5
			 * IF((Bardzo Wolno) && (Srednia) && Srednia) THEN Przysp.Mocno i omin
			 */
		    miAgregacja.set(2,miAtrybutWagaSrednia);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			
			
			/** Regula 6
			 * IF((Bardzo Wolno) && (Srednia) && Duza) THEN Przysp.Mocno i omin
			 */
		    miAgregacja.set(2,miAtrybutWagaDuza);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			
		    
			
			/** Regula 7
			 *  IF((Bardzo Wolno) && (Duza) && Mala) THEN Przysp.Mocno i omin
			 */
		    
		    miAgregacja.set(1,miWielkoscPrzeszkodyDuza);
		    miAgregacja.set(2,miAtrybutWagaMala);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			
			/** Regula 8
			*  IF((Bardzo Wolno) && (Duza) && Srednia) THEN Przysp.Mocno i omin
			 */
		    
		    miAgregacja.set(2,miAtrybutWagaSrednia);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			
			/** Regula 9
			*  IF((Bardzo Wolno) && (Duza) && Duza) THEN Przysp.Mocno i omin
			*/
		    
		    miAgregacja.set(2,miAtrybutWagaDuza);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			
			
		    
		    /** Regula 10
			 * IF(Wolno && ( Mala ) && Mala) THEN Przysp.Mocno i omin
			 */
		  
		    miAgregacja.set(0,miMojaPredkoscMala);
			miAgregacja.set(1,miWielkoscPrzeszkodyMala);
			miAgregacja.set(2,miAtrybutWagaMala);
			mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    
		    /** Regula 11
			 *  IF(Wolno && ( Mala ) && Srednia) THEN Przysp.Mocno i taranuj
			 */
		    
			miAgregacja.set(2,miAtrybutWagaSrednia);
			mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    
		    /** Regula 12
			 *  IF(Wolno && ( Mala ) && Duza) THEN Przysp.Mocno i taranuj
			 */
		    
			miAgregacja.set(2,miAtrybutWagaDuza);
			mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    
		    /** Regula 13
			 *  IF(Wolno && ( Srednia ) && Mala) THEN Przysp.Mocno i omin
			 */
		    
			 miAgregacja.set(1,miWielkoscPrzeszkodySrednia);
			 miAgregacja.set(2,miAtrybutWagaMala);
			 mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			 regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
				
		    /** Regula 14
			 *  IF(Wolno && ( Srednia ) && Srednio) THEN Przysp.Mocno i omin
			 */
		    
			miAgregacja.set(2,miAtrybutWagaSrednia);
			mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    
		    /** Regula 15
			 * IF(Wolno && ( Srednia ) && Duza) THEN Przysp.Mocno i taranuj
		    */
		    
			miAgregacja.set(2,miAtrybutWagaDuza);
			mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			/** Regula 16
			 * IF(Wolno && ( Duza ) && Mala) THEN Przysp.Mocno i omin
			 */
		    
			miAgregacja.set(1,miWielkoscPrzeszkodyDuza);
			miAgregacja.set(2,miAtrybutWagaMala);
			mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    
			/** Regula 17
			 * IF(Wolno && ( Duza ) && Srednia) THEN Przysp.Mocno i omin
			 */
		    miAgregacja.set(2,miAtrybutWagaSrednia);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    
		    /** Regula 18
			 * IF(Wolno && ( Duza ) && Duza) THEN Przysp.Mocno i omin
			 */
		    
		    miAgregacja.set(2,miAtrybutWagaDuza);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    
		    /** Regula 19
			 * IF(Srednio && ( Mala ) && Mala) THEN Przysp.Mocno i taranuj
			 */
		    
		    miAgregacja.set(0,miMojaPredkoscSrednia);
			miAgregacja.set(1,miWielkoscPrzeszkodyMala);
			miAgregacja.set(2,miAtrybutWagaMala);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    /** Regula 20
			 * IF(Srednio && ( Mala ) && Srednia) THEN Przysp.Mocno i taranuj
			 */
		    
		    miAgregacja.set(2,miAtrybutWagaSrednia);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    /** Regula 21
			 * IF(Srednio && ( Mala ) && Duza) THEN Przysp.Mocno i taranuj
			 */
		    miAgregacja.set(2,miAtrybutWagaDuza);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    /** Regula 22
			 * IF(Srednio && ( Srednia ) && Mala) THEN Przysp.Srednio i omin
			 */
		    miAgregacja.set(1,miWielkoscPrzeszkodySrednia);
			miAgregacja.set(2,miAtrybutWagaMala);
		    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyOmin.add( new PozycjaReguly(Lista.SREDNIO, sredniePrzyspieszenie.size()-1)); 
		    
		    /** Regula 23
			 * IF(Srednio && ( Srednia ) && Srednia) THEN Przysp.Mocno i taranuj
			 */
		    miAgregacja.set(2,miAtrybutWagaSrednia);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    /** Regula 24
			 * IF(Srednio && ( Srednia ) && Duza) THEN Przysp.Mocno i taranuj
			 */
		    miAgregacja.set(2,miAtrybutWagaDuza);
		    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
		    regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
		    /** Regula 25
			 * IF(Srednio && ( Duza) && Mala) THEN Przysp.Srednio i omin
			 */
		    miAgregacja.set(1,miWielkoscPrzeszkodyDuza);
			miAgregacja.set(2,miAtrybutWagaMala);
			sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			regulyOmin.add( new PozycjaReguly(Lista.SREDNIO, sredniePrzyspieszenie.size()-1)); 
			 
		    /** Regula 26
			 * IF(Srednio && ( Duza ) && Srednia) THEN Przysp.Srednio i omin
			 */
			 miAgregacja.set(2,miAtrybutWagaSrednia);
			 sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			 regulyOmin.add( new PozycjaReguly(Lista.SREDNIO, sredniePrzyspieszenie.size()-1)); 
			 
		    /** Regula 27
			 * IF(Srednio && ( Duza ) && Duza) THEN Przysp.Srednio i omin
			 */
			 miAgregacja.set(2,miAtrybutWagaDuza);
			 sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			 regulyOmin.add( new PozycjaReguly(Lista.SREDNIO, sredniePrzyspieszenie.size()-1)); 
			 
			 /** Regula 28
				 * IF(Szybko && ( Mala ) && Mala) THEN Przysp.Mocno i taranuj
				 */
				miAgregacja.set(0,miMojaPredkoscDuza);
				miAgregacja.set(1,miWielkoscPrzeszkodyMala);
				miAgregacja.set(2, miAtrybutWagaMala);
			    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			    regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
				
				
				/** Regula 29
				 * IF(Szybko && (Mala) && Srednie) THEN Przysp.Mocno i taranuj
				 */
			    miAgregacja.set(2,miAtrybutWagaSrednia);
			    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			    regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			    
			    
				/** Regula 30
				 * IF(Szybko && (Mala) && Duza) THEN Przysp.Mocno i taranuj
				 */
			    miAgregacja.set(2,miAtrybutWagaDuza);
			    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			    regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			    
				
				/** Regula 31
				 * IF(Szybko && (Srednia) && Mala) THEN Przysp.Srednio i taranuj
				 */
			    miAgregacja.set(1,miWielkoscPrzeszkodySrednia);
			    miAgregacja.set(2,miAtrybutWagaMala);
			    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			    regulyTaranuj.add( new PozycjaReguly(Lista.SREDNIO, mocnePrzyspieszenie.size()-1)); 
				/** Regula 32
				 * IF(Szybko && (Srednia) && Srednia) THEN Przysp.Srednio i taranuj
				 */
			    miAgregacja.set(2,miAtrybutWagaSrednia);
			    mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			    regulyOmin.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
				
				
				/** Regula 33
				 * IF(Szybko && (Srednia) && Duza) THEN Przysp.Mocno i taranuj
				 */
			    miAgregacja.set(2,miAtrybutWagaDuza);
			    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			    regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			    
				
				/** Regula 34
				 *  IF(Szybko && (Duza) && Mala) THEN Przysp.Srednio i taranuj
				 */
			    
			    miAgregacja.set(1,miWielkoscPrzeszkodyDuza);
			    miAgregacja.set(2,miAtrybutWagaMala);
			    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			    regulyTaranuj.add( new PozycjaReguly(Lista.SREDNIO, mocnePrzyspieszenie.size()-1)); 
				/** Regula 35
				*  IF(Szybko && (Duza) && Srednia) THEN Przysp.Srednio i taranuj
				 */
			    
			    miAgregacja.set(2,miAtrybutWagaSrednia);
			    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			    regulyTaranuj.add( new PozycjaReguly(Lista.SREDNIO, mocnePrzyspieszenie.size()-1)); 
				/** Regula 36
				*  IF(Szybko && (Duza) && Duza) THEN Przysp.Srednio i taranuj
				*/
			    
			    miAgregacja.set(2,miAtrybutWagaDuza);
			    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			    regulyTaranuj.add( new PozycjaReguly(Lista.SREDNIO, mocnePrzyspieszenie.size()-1)); 
				
			    /** Regula 37
				 * IF(Bardzo Szybko && ( Mala ) && Mala) THEN Przysp.Srednio i taranuj
				 */
			  
			    miAgregacja.set(0,miMojaPredkoscBardzoDuza);
				miAgregacja.set(1,miWielkoscPrzeszkodyMala);
				miAgregacja.set(2,miAtrybutWagaMala);
				sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
				regulyTaranuj.add( new PozycjaReguly(Lista.SREDNIO, mocnePrzyspieszenie.size()-1)); 
			    /** Regula 38
				 *  IF(Bardzo Szybko && ( Mala ) && Srednia) THEN Przysp.Mocno i taranuj
				 */
			    
				miAgregacja.set(2,miAtrybutWagaSrednia);
				mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
				System.err.println("reg 38  " + miAgregacja); 
				regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			    
			    /** Regula 39
				 *  IF(Bardzo Szybko && ( Mala ) && Duza) THEN Przysp.Mocno i taranuj
				 */
			    
				miAgregacja.set(2,miAtrybutWagaDuza);
				mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
				regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
			    /** Regula 40
				 *  IF(Bardzo Szybko && ( Srednia ) && Mala) THEN Przysp.Srednio i omin
				 */
			    
				 miAgregacja.set(1,miWielkoscPrzeszkodySrednia);
				 miAgregacja.set(2,miAtrybutWagaMala);
				 sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
				 regulyOmin.add( new PozycjaReguly(Lista.SREDNIO, mocnePrzyspieszenie.size()-1)); 
			    /** Regula 41
				 *  IF(Bardzo Szybko && ( Srednia ) && Srednio) THEN Przysp.Srednio i taranuj
				 */
			    
				miAgregacja.set(2,miAtrybutWagaSrednia);
				sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
				regulyTaranuj.add( new PozycjaReguly(Lista.SREDNIO, mocnePrzyspieszenie.size()-1)); 
			    /** Regula 42
				 * IF(Bardzo Szybko && ( Srednia ) && Duza) THEN Przysp.Mocno i taranuj
			    */
			    
				miAgregacja.set(2,miAtrybutWagaDuza);
				mocnePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
				regulyTaranuj.add( new PozycjaReguly(Lista.MOCNO, mocnePrzyspieszenie.size()-1)); 
				/** Regula 43
				 * IF(Bardzo Szybko && ( Duza ) && Mala) THEN Przysp.Slabo i taranuj
				 */
			    
				miAgregacja.set(1,miWielkoscPrzeszkodyDuza);
				miAgregacja.set(2,miAtrybutWagaMala);
			    slabePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			    regulyTaranuj.add( new PozycjaReguly(Lista.SLABO, mocnePrzyspieszenie.size()-1)); 
				/** Regula 44
				 * IF(Bardzo Szybko && ( Duza ) && Srednia) THEN Przysp.Srednio i taranuj
				 */
			    miAgregacja.set(2,miAtrybutWagaSrednia);
			    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			    regulyTaranuj.add( new PozycjaReguly(Lista.SREDNIO, mocnePrzyspieszenie.size()-1)); 
			    
			    /** Regula 45
				 * IF(Bardzo Szybko && ( Duza ) && Duza) THEN Przysp.Srednio i taranuj
				 */
			    
			    miAgregacja.set(2,miAtrybutWagaDuza);
			    sredniePrzyspieszenie.add( SpojnikiLogiczne.AND(miAgregacja));
			    regulyTaranuj.add( new PozycjaReguly(Lista.SREDNIO, mocnePrzyspieszenie.size()-1)); 
			    bardzoSlabePrzyspieszenie.add(-10.0);
			double przyspieszenie = Przyspieszenie.agregacja(bardzoSlabePrzyspieszenie,slabePrzyspieszenie, sredniePrzyspieszenie, mocnePrzyspieszenie);
			
			myCar.setAccelerationPercent((int) przyspieszenie);
			myCar.setBrakePercent(0);
			

			PozycjaReguly tmpPoz3 = new PozycjaReguly(Przyspieszenie.ktoraLista, Przyspieszenie.ktoryIndekswLiscie);
			if(regulyOmin.contains(tmpPoz3)){
				myCar.setTarget(AIUtils.getAlternativeLane(myCar.getCheckpoint(), myCar.getPosition()));
				//System.err.println("alternative lane" + tmpPoz3.lista  + tmpPoz3.pozycjawLiscie  ); 
			}
			else {
				myCar.setTarget(AIUtils.getClosestLane(myCar.getCheckpoint(), myCar.getPosition()));
				//System.err.println("closest lane "); 
			}
			Przyspieszenie.DEBUG = false;
		
		}
}

