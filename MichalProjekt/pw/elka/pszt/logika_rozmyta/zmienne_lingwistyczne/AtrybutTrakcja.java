package pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne;

public class AtrybutTrakcja {

	static public double czySlaba(double f){
		if( f < 0.05){
			return 1.0;
		}
		else if(f >= 0.05 && f < 0.1){
			return -20*f +2;
		}
		else{
			return 0.0;
		}
	}	
	static public double czySrednia(double f){
		if(f < 0.05 || f >= 0.2 ){
			return 0.0;
		}
		else if (f >= 0.05 && f < 0.1){
			return 20*f-1;
		}
		else if (f >= 0.1 && f < 0.15){
			return 1.0;
		}
		else{
			return -0.2*f -8;
		}
	}
	static public double czyMocna(double f){

		 if(f >= 0.15 && f < 0.2){
			return 0.2*f-8;
		}
		else if(f >= 0.2){
			return 1.0;
		}
		else{
			return 0.0;
		}
	}
}