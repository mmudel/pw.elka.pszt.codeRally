package pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne;

public class AtrybutWaga {
	

	
	static public double czyMala(double f){
		if(f <= 5000){
			return 1.0;
		}
		else if(f > 5000 && f < 7000){
			return -(f/2000) + 3.5;
		}
		else
			return 0.0;
	}
	
	
	
	
	static public double czySrednia(double f){
		if(f <= 5000 || f >= 12000){
			return 0.0;
		}
		else if(f > 5000 && f < 7000){
			return f/2000 - 2.5;
		}
		else if(f >= 7000 && f <= 9000){
			return 1.0;
		}
		else{
			return -(f/3000) + 4;
		}
	}	
	
	
	
	
	static public double czyDuza(double f){
		if( f <= 9000 ){
			return 0.0;
		}
		else if (f > 9000 && f < 12000){
			return f/3000 - 3;
		}
		else{
			return 1.0;
		}
	}
	

}



