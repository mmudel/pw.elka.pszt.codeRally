package pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne;

import java.util.List;

import pw.elka.pszt.logika_rozmyta.utils.Przedzial;


public class Zmienna{
	public static boolean DEBUG;
	public static double agregacja(List<Double> wartosciDlaBardzoSlabo, List<Double> wartosciDlaSlabo, List<Double> wartosciDlaSrednio, List<Double> wartosciDlaMocno){ return 0.0;}
    
	protected static abstract class funkcjePrzedzialow {
		public funkcjePrzedzialow(double m1, double m2) {
			miF1 = m1;
			miF2 = m2;
		}
		public abstract Przedzial f1(double mi);
		public abstract Przedzial f2(double mi);
		public double miF1;
		public double miF2;
	}
	
	protected static  class bSlaboSlabo extends funkcjePrzedzialow{
		public bSlaboSlabo(double m1, double m2) {
			super(m1, m2);
		}
		public Przedzial f1(double mi){
			Przedzial przedzial = new Przedzial();
			przedzial.x1=0;
			przedzial.x2=(-2*mi+5);
			return przedzial;
		}
		public Przedzial f2(double mi){
			Przedzial przedzial = new Przedzial();
			przedzial.x1 = 2*mi+3;
			przedzial.x2 = (-20*mi+40);
			return przedzial;
		}
	}
	protected static class SlaboSrednio extends funkcjePrzedzialow{
		public SlaboSrednio(double m1, double m2) {
			super(m1, m2);
		}
		public Przedzial f1(double mi){
			Przedzial przedzial = new Przedzial();
			przedzial.x1 = 2*mi+3;
			przedzial.x2 = (-20*mi+40);
			return przedzial;
		}
		public Przedzial f2(double mi){
			Przedzial przedzial = new Przedzial();
			przedzial.x1 = (20*mi+20);
			przedzial.x2 = (-20*mi+80);
			return przedzial;
		}
	}
	protected static class SrednioMocno extends funkcjePrzedzialow{
		public SrednioMocno(double m1, double m2) {
			super(m1, m2);
		}
		public Przedzial f1(double mi){
			Przedzial przedzial = new Przedzial();
			przedzial.x1 = (20*mi+20);
			przedzial.x2 = (-20*mi+80);
			return przedzial;
		}
		public Przedzial f2(double mi){
			Przedzial przedzial = new Przedzial();
			przedzial.x2 = 100;
			przedzial.x1 = (20*mi+60);
			return przedzial;
		}
	}
	
	protected static double srodekCiezkosci(double mibSlabo, double miSlabo, double miSrednio, double miMocno){
		double sumaMianownik = 0, sumaLicznik = 0;
		
		sumaLicznik += obliczeniaLicznik( new bSlaboSlabo(mibSlabo, miSlabo), sumaMianownik, punktPrzecieciaBSlaboSlaboX);
		sumaLicznik += obliczeniaLicznik( new SlaboSrednio(miSlabo, miSrednio), sumaMianownik, punktPrzecieciaSlaboSrednioX);
		sumaLicznik += obliczeniaLicznik( new SrednioMocno(miSrednio, miMocno), sumaMianownik, punktPrzecieciaSrednioMocnoX);
		sumaLicznik += sumaMianownik += 2*mibSlabo + 2*miSlabo + 2*miSrednio + 2*miMocno;
		return sumaLicznik/sumaMianownik;
	}
	
	static protected double obliczeniaLicznik(funkcjePrzedzialow fun, Double sumaMianownik, double pktPrzecX){
         Przedzial przedF1, przedF2; 
		double sumaLicznik = 0;
		if(fun.miF1 > punktPrzecieciaWszystkieY && fun.miF2 > punktPrzecieciaWszystkieY){
			przedF1 = fun.f1(fun.miF1);
			przedF2 = fun.f2(fun.miF2);
			sumaLicznik += fun.miF1 * przedF1.x1;
			sumaLicznik += fun.miF1 * przedF1.x2;
			sumaLicznik += fun.miF2 * przedF2.x1;
			sumaLicznik += fun.miF2 * przedF2.x2;
			sumaLicznik +=punktPrzecieciaWszystkieY * pktPrzecX;
			sumaMianownik += punktPrzecieciaWszystkieY;
		}
		else if (fun.miF1 > fun.miF2){
			przedF1 = fun.f1(fun.miF1);
			sumaLicznik += fun.miF1 * przedF1.x1;
			sumaLicznik += fun.miF1 * przedF1.x2;
			przedF2 = fun.f1(fun.miF2);
			sumaLicznik += fun.miF2 * przedF2.x1;
			przedF2 = fun.f2(fun.miF2);
			sumaLicznik += fun.miF2 * przedF2.x1;
		}
		else{
			przedF2 = fun.f2(fun.miF2);
			sumaLicznik += fun.miF2 * przedF2.x1;
			sumaLicznik += fun.miF2 * przedF2.x2;
			przedF1 = fun.f2(fun.miF1);
			sumaLicznik += fun.miF1 * przedF1.x1;
			przedF1 = fun.f1(fun.miF1);
			sumaLicznik += fun.miF1 * przedF1.x1;
		}
		return sumaLicznik;
	}
	
	static protected double punktPrzecieciaBSlaboSlaboX = 4;
	static protected double punktPrzecieciaSlaboSrednioX = 30;
	static protected double punktPrzecieciaSrednioMocnoX = 70;
	static protected double punktPrzecieciaWszystkieY = 0.5;
}
