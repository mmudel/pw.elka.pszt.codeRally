package pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne;

public class AtrybutPrzyspieszenie {

	static public double czySlabe(double f){
		if( f < 12){
			return 1.0;
		}
		else if(f >= 12 && f < 32){
			return -0.05*f + 1.6;
		}
		else{
			return 0.0;
		}
	}	
	static public double czySrednie(double f){
		if(f < 12 || f >=45 ){
			return 0.0;
		}
		else if (f >= 12 && f < 32){
			return 0.05*f - 0.6;
		}
		else if (f >= 32 && f < 40){
			return 1.0;
		}
		else{
			return -0.2*f+9;
		}
	}
	static public double czyMocne(double f){

		 if(f >= 40 && f < 45){
			return 0.2*f -8;
		}
		else if(f >= 45){
			return 1.0;
		}
		else{
			return 0.0;
		}
	}
}