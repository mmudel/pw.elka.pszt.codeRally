package pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne;

public class IleTrzebaSkrecic {
	static public double czyMalo(double f){
		f = Math.abs(f);
		if(f <= 5){
			return 1.0;
		}
		else if(f > 5 && f < 15){
			return -(f/10) + 1.5;
		}
		else{
			return 0.0;
		}
	}
	static public double czySrednio(double f){
		f = Math.abs(f);
		if(f <= 5 || f >= 60){
			return 0.0;
		}
		else if(f > 5 && f < 15){
			return f/10 - 0.5;
		}
		else if(f >= 15 && f <= 45){
			return 1.0;
		}
		else{
			return -(f/15) + 4.0;
		}
	}
	static public double czyDuzo(double f){
		f = Math.abs(f);
		if(f <= 45 || f >= 110)
			return 0.0;
		else if(f > 45 && f < 60){
			return f/15 - 3.0;
		}
		else if(f >= 60 && f <= 90){
			return 1.0;
		}
		else{
			return -(f/20)+5.5;
		}
	}
	static public double czyBardzoDuzo(double f){
		f = Math.abs(f);
		if(f <= 90){
			return 0.0;
		}
		else if(f > 90 && f < 110){
			return f/20 - 4.5;
		}
		else{
			return 1.0;
		}
	}
	
}
