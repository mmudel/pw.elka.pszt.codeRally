package pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne;

import java.util.Collections;
import java.util.List;

import pw.elka.pszt.logika_rozmyta.utils.*;

public class Przyspieszenie extends Zmienna {
	
	/* Agregacja za pomoca wyznaczenia srodka ciezkosci */
	static public Lista ktoraLista;
	static public int ktoryIndekswLiscie;
	static public boolean DEBUG = false;
	static public double agregacja(List<Double> wartosciDlaBardzoSlabo, List<Double> wartosciDlaSlabo, List<Double> wartosciDlaSrednio, List<Double> wartosciDlaMocno){
		Double maxWartoscDlaBardzoSlabo = new Double(0);
		Double maxWartoscDlaSlabo = new Double(0);
		Double maxWartoscDlaSrednio = new Double(0);
		Double maxWartoscDlaMocno = new Double(0);
		
		if(!wartosciDlaBardzoSlabo.isEmpty()) maxWartoscDlaBardzoSlabo = Collections.max(wartosciDlaBardzoSlabo);
		if(!wartosciDlaSlabo.isEmpty()) maxWartoscDlaSlabo = Collections.max(wartosciDlaSlabo);
		if(!wartosciDlaSrednio.isEmpty()) maxWartoscDlaSrednio = Collections.max(wartosciDlaSrednio);
		if(!wartosciDlaMocno.isEmpty()) maxWartoscDlaMocno = Collections.max(wartosciDlaMocno);
		
		if(maxWartoscDlaBardzoSlabo >= maxWartoscDlaSlabo && maxWartoscDlaBardzoSlabo >= maxWartoscDlaSrednio &&
				maxWartoscDlaBardzoSlabo >= maxWartoscDlaMocno){
			
			ktoraLista = Lista.BSLABO;
			ktoryIndekswLiscie = wartosciDlaBardzoSlabo.indexOf(maxWartoscDlaBardzoSlabo);
			if(DEBUG)
			{
				//for(double d: wartosciDlaMocno )
					//System.err.println("Wybrano bardzo slabe przyspieszenie: " + d);
				System.err.println("Wybrano bardzo slabe przyspieszenie: " +  ktoryIndekswLiscie + maxWartoscDlaBardzoSlabo +" "+ maxWartoscDlaSlabo +" "+  wartosciDlaSrednio.size() +" "+ wartosciDlaMocno.size());
			}
				
		}
		else if(maxWartoscDlaSlabo >= maxWartoscDlaBardzoSlabo && maxWartoscDlaSlabo >= maxWartoscDlaSrednio && maxWartoscDlaSlabo >= maxWartoscDlaMocno){
			ktoraLista = Lista.SLABO;
			ktoryIndekswLiscie = wartosciDlaSlabo.indexOf(maxWartoscDlaSlabo);
			if(DEBUG)
				System.err.println("Wybrano slabe przyspieszenie:" +  ktoryIndekswLiscie);
		}
		else if(maxWartoscDlaSrednio >= maxWartoscDlaBardzoSlabo && maxWartoscDlaSrednio >= maxWartoscDlaSlabo && maxWartoscDlaSrednio >= maxWartoscDlaMocno){
			
			ktoraLista = Lista.SREDNIO;
			ktoryIndekswLiscie = wartosciDlaSrednio.indexOf(maxWartoscDlaSrednio);
			if(DEBUG)
				System.err.println("Wybrano srednie przyspieszenie: " +  ktoryIndekswLiscie);
		}
		else {
			
			ktoraLista = Lista.MOCNO;
			ktoryIndekswLiscie = wartosciDlaMocno.indexOf(maxWartoscDlaMocno);
			if(DEBUG)
				System.err.println("Wybrano mocne przyspieszenie: " +  ktoryIndekswLiscie);
		}
		return srodekCiezkosci(maxWartoscDlaBardzoSlabo, maxWartoscDlaSlabo, maxWartoscDlaSrednio, maxWartoscDlaMocno);
	}
	
	
}

