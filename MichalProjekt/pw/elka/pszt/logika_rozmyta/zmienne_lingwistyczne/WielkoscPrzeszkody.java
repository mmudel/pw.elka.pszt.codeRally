package pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne;


public class WielkoscPrzeszkody {
	
	
	static public double czyMala(double f){
		
		if(f <= 30){
			return 1.0;
		}
		else if(f > 30 && f < 40){
			return -(f/10) + 4;
		}
		else
			return 0.0;
	}
	
	
	
	
	static public double czySrednia(double f){
		if(f <= 30 || f >= 100){
			return 0.0;
		}
		else if(f > 30 && f < 40){
			return f/10 - 3;
		}
		else if(f >= 40 && f <= 60){
			return 1.0;
		}
		else{
			return -(f/40) + 2.5;
		}
	}	
	
	
	
	
	static public double czyDuza(double f){
		if( f <= 60 ){
			return 0.0;
		}
		else if (f > 60 && f < 100){
			return f/40 - 1.5;
		}
		else{
			return 1.0;
		}
	}
	

}
