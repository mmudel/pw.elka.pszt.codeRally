package pw.elka.pszt.logika_rozmyta.zmienne_lingwistyczne;


public class Predkosc {
	
	static public double czyBardzoWolno(double f){
		if(f <= 15){
			return 1.0;
		}
		else if(f > 15 && f < 20){
			return -(f/5) + 4;
		}
		else
			return 0.0;
	}
	static public double czyWolno(double f){
		if(f <= 15 || f >= 70){
			return 0.0;
		}
		else if(f > 15 && f < 20){
			return f/5 - 3;
		}
		else if(f >= 20 && f <= 50){
			return 1.0;
		}
		else{
			return -(f/20) + 3.5;
		}
	}	
	static public double czySrednio(double f){
		if(f <= 50 || f >= 120){
			return 0.0;
		}
		else if (f > 50 && f < 70){
			return f/20 - 2.5;
		}
		else if (f >= 70 && f <= 100){
			return 1.0;
		}
		else{
			return -(f/20) + 6;
		}
	}
	static public double czySzybko(double f){
		if(f <= 100 || f >= 200){
			return 0.0;
		}
		else if(f > 100 && f < 120){
			return f/20 - 5;
		}
		else if(f >= 120 && f <= 180){
			return 1.0;
		}
		else{
			return -(f/20) + 10;
		}
	}
	static public double czyBardzoSzybko(double f){
		if(f <= 180){
			return 0.0;
		}
		else if(f > 180 && f < 200){
			return f/20 - 9;
		}
		else{
			return 1.0;
		}
	}
}