package pw.elka.pszt.logika_rozmyta.utils;

import java.util.Collections;
import java.util.List;

public class SpojnikiLogiczne {
	
	static public double OR(List<Double> wartosci){
		return Collections.max(wartosci);
	}
	static public double AND(List<Double> wartosci){
		return Collections.min(wartosci);
	}
	static public double NOT(double wartosc){
		return 1.0 - wartosc;
	}
}